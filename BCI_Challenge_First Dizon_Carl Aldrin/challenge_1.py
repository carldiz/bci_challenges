"""
Difficulty: easy
"""

#from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import pandas as pd
from datetime import datetime

url = 'https://www.emservices.com.sg/tenders/' # URL of the website that will be web scrapped
data = []

r = requests.get(url)
soup = BeautifulSoup(r.content, 'html.parser')
html = list(soup.children)[3] # This will take the html code of the url
table = html.tbody
body = table.find_all('tr') # Extraction of html body with the important line code
for row in body:
    content = row.find_all('td') # Extraction of html element that starts with 'td'
    # The following code will output the datetime of the html file
    advert_date = content[0].text
    # This code will parse the datetime into day/month/year
    advert_date_dt = datetime.strptime(advert_date, '%d/%m/%Y')
    closing_date = content[1].text
    closing_date_dt = datetime.strptime(closing_date, '%d/%m/%Y')

    # This code will extract the necessary information about the web page
    client = content[2].text
    description = content[3].text
    eligibility = content[4].text
    
    # This code will get the url link about the client
    links = row.find_all('a')
    link = links[0].get('href')
    
    # This will put all the data collected into a table list called 'data'
    data.append([advert_date_dt, closing_date_dt, client, description, eligibility, link])

# This code will set the certain column of the dataframe and will import the scrapped data into a csv file.
df = pd.DataFrame(data, columns=['advert_date', 'closing_date', 'client', 'description', 'eligibility', 'link'])
df.to_csv('BCI_Challenge_First.csv', index=False)

