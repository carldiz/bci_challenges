"""
Difficulty: medium
"""

#from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import pandas as pd

url = 'https://businesspapers.parracity.nsw.gov.au/' # URL of the website that will be web scrapped
data = []

r = requests.get(url)
soup = BeautifulSoup(r.content, 'html.parser')
html = list(soup.children)[4] # This will take the html code of the url
body = html.find('tbody').find_all('tr')
for row in body:
    # This code will extract the datetime value of the website
    content = row.find_all('td')
    dnt = content[0].get_text(separator=' ').strip() # This code will separate the date and time from <br/> line

    # This code will put a unique string to line break then will proceed to split
    # the line to extract the meeting description
    meeting_desc = content[1].get_text(separator='@').strip().split('@') 

    # A dictionary that is used for html link extraction
    links_dict = {
        'agenda_html': None,
        'agenda_pdf': None,
        'minutes_html': None
    } 
    links = row.find_all('a')
    
    # This code is an elif statement wherein a specific element would match the parameter for link extraction
    for link in links:
        if 'Agenda HTML' in link.text:
            links_dict['agenda_html'] = url + link.get('href')
        elif 'Agenda PDF' in link.text:
            links_dict['agenda_pdf'] = url + link.get('href')
        elif 'Minutes HTML' in link.text:
            links_dict['minutes_html'] = url + link.get('href')
    
    # This will put all the data collected into a table list called 'data'
    data.append([dnt, meeting_desc[0], links_dict['agenda_html'], links_dict['agenda_pdf'], links_dict['minutes_html']])
    
# This code will set the certain column of the dataframe and will import the scrapped data into a csv file.
df = pd.DataFrame(data, columns=['meeting_date', 'meeting_description', 'agenda_html_link', 'agenda_pdf_link', 'minutes'])
df.to_csv('BCI_Challenge_Second.csv', index=False)
