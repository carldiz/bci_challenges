"""
Difficulty: hard
"""

#from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import pandas as pd

url = 'https://infrastructuremagazine.com.au/news/' # URL of the website that will be web scrapped
data = []

r = requests.get(url)
soup = BeautifulSoup(r.content, 'html.parser')
body = list(soup.children)[12] # This will take the html code of the url
articles = body.find_all('article') # Extraction of html body with the important line code
for article in articles:
    # The following code will extract the data from the website of the following information:
    title = article.find('h3').text
    description = article.find('p').text
    publish_date = article.find('time').get('datetime')
    link = article.find('a').get('href')
    
    # This will put all the data collected into a table list called 'data'
    data.append([title, description, publish_date, link])
    
# This code will set the certain column of the dataframe and will import the scrapped data into a csv file.
df = pd.DataFrame(data, columns=['title', 'description', 'publish_date', 'link'])
df.to_csv('BCI_Challenge_Fourth.csv', index=False)